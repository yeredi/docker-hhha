Instalación

1- Ejecutar compilación de imagen docker Oracle con el tag "oracle", es imporante compliar con el tag definido ya que los php son instalados desde la imagen de Oracle.

`docker build -t oracle -f Docker-oracle .`

2- Ejecutar compilación de PHP (No importa el orden)

`docker build -t hhha-php5 -f Docker-web-php5 .`

`docker build -t hhha-php7 -f Docker-web-php7-apache .`

3- Crear contenedor

`docker run -p 8000:80 --name php5 -v /home/andres/Apps/hhha_php5:/home hhha-php5`

`docker run -p 8000:80 --name php7 -v /home/andres/Apps/hhha_php7:/var/www/html hhha-php7`

Ejecutar comandos dentro del contenedor

`docker run -p 8000:80 --name php5 -v /home/andres/Apps/hhha_php5:/home hhha-php5`

`docker run -p 80:80 --name php7 -v /home/andres/Apps/hhha_php7:/home hhha-php7`
